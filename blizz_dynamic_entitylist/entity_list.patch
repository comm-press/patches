diff --git a/src/Plugin/Field/FieldFormatter/BlizzDynamicEntitylistFormatter.php b/src/Plugin/Field/FieldFormatter/BlizzDynamicEntitylistFormatter.php
index 7079b03..f6d86ad 100644
--- a/src/Plugin/Field/FieldFormatter/BlizzDynamicEntitylistFormatter.php
+++ b/src/Plugin/Field/FieldFormatter/BlizzDynamicEntitylistFormatter.php
@@ -2,6 +2,7 @@
 
 namespace Drupal\blizz_dynamic_entitylist\Plugin\Field\FieldFormatter;
 
+use Drupal\blizz_dynamic_entitylist\Services\BlizzDynamicEntitylistApiInterface;
 use Drupal\blizz_dynamic_entitylist\Services\BlizzDynamicEntitylistWidgetServicesInterface;
 use Drupal\blizz_dynamic_entitylist\Services\EntityServicesInterface;
 use Drupal\Core\Field\FieldItemListInterface;
@@ -11,6 +12,7 @@ use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
 use Drupal\Core\Routing\RouteMatchInterface;
 use Symfony\Component\DependencyInjection\ContainerInterface;
 use Symfony\Component\HttpFoundation\RequestStack;
+use Drupal\Core\Url;
 
 /**
  * Plugin implementation of the 'blizz_dynamic_entitylist' formatter.
@@ -53,6 +55,13 @@ class BlizzDynamicEntitylistFormatter extends FormatterBase implements Container
    */
   protected $entitylistWidgetServices;
 
+  /**
+   * Custom service to exclude entities from lists by id.
+   *
+   * @var \Drupal\blizz_dynamic_entitylist\Services\BlizzDynamicEntitylistApiInterface
+   */
+  protected $api;
+
   /**
    * {@inheritdoc}
    */
@@ -64,7 +73,8 @@ class BlizzDynamicEntitylistFormatter extends FormatterBase implements Container
       $container->get('request_stack'),
       $container->get('current_route_match'),
       $container->get('blizz_dynamic_entitylist.entity_services'),
-      $container->get('blizz_dynamic_entitylist.field_widget_service')
+      $container->get('blizz_dynamic_entitylist.field_widget_service'),
+      $container->get('blizz_dynamic_entitylist.api')
     );
   }
 
@@ -93,7 +103,8 @@ class BlizzDynamicEntitylistFormatter extends FormatterBase implements Container
     RequestStack $request_stack,
     RouteMatchInterface $route_match,
     EntityServicesInterface $entity_services,
-    BlizzDynamicEntitylistWidgetServicesInterface $entity_list_widget_services
+    BlizzDynamicEntitylistWidgetServicesInterface $entity_list_widget_services,
+    BlizzDynamicEntitylistApiInterface $api
   ) {
     parent::__construct(
       $plugin_id,
@@ -108,6 +119,7 @@ class BlizzDynamicEntitylistFormatter extends FormatterBase implements Container
     $this->routeMatcher = $route_match;
     $this->entityServices = $entity_services;
     $this->entitylistWidgetServices = $entity_list_widget_services;
+    $this->api = $api;
   }
 
   /**
@@ -189,7 +201,7 @@ class BlizzDynamicEntitylistFormatter extends FormatterBase implements Container
    * {@inheritdoc}
    */
   public function viewElements(FieldItemListInterface $items, $langcode) {
-
+    $parameters = $this->routeMatcher->getParameters();
     $listdefinition = $this->entitylistWidgetServices->getListDefinition($items->get(0)->getValue());
 
     if (!empty($listdefinition)) {
@@ -262,6 +274,55 @@ class BlizzDynamicEntitylistFormatter extends FormatterBase implements Container
           );
         }
 
+        // Load more button.
+        if (isset($listdefinition['load_more']) && $listdefinition['load_more']) {
+          $offset = $parameters->get('offset') ? $parameters->get('offset') : $listdefinition['offset'];
+          $offset += $listdefinition['number_of_load'];
+          $length = $listdefinition['length'];
+          if (count($renderarray) && (!$length || $length > $offset)) {
+            $id = "ajax-list-{$this->fieldDefinition->getTargetEntityTypeId()}-{$items->getEntity()->id()}-{$this->fieldDefinition->getName()}";
+
+            $renderarray['actions'] = [
+              '#type' => 'html_tag',
+              '#tag' => 'div',
+              '#attributes' => [
+                'class' => [
+                  'text-align-center',
+                  'blizz-dynamic-ajax',
+                ],
+                'id' => $id
+              ],
+            ];
+            $renderarray['actions']['more'] = [
+              '#type' => 'link',
+              '#title' => t('Load More'),
+              '#url' => Url::fromRoute('blizz_dynamic_entitylist.ajax-list', [
+                'entity_type' => $this->fieldDefinition->getTargetEntityTypeId(),
+                'entity' => $items->getEntity()->id(),
+                'field_name' => $this->fieldDefinition->getName(),
+                'view_mode' => $this->viewMode,
+                'language' => 'de',
+                'offset' => $offset,
+                'exclude' => $this->api->getExcludedEntities($listdefinition['entity_type_id'])
+              ]),
+              '#attributes' => [
+                'class' => [
+                  'use-ajax',
+                ],
+              ],
+            ];
+            $renderarray['#attached']['library'][] = 'core/drupal.ajax';
+
+            // Automatically Load Content.
+            if (isset($listdefinition['infinite_scroll']) && $listdefinition['infinite_scroll']) {
+              $renderarray['actions']['#attributes']['class'][] = 'infinite-scroll';
+              $renderarray['#attached']['library'][] = 'blizz_dynamic_entitylist/BlizzDynamicEntitylistInfiniteScroll';
+            }
+
+          }
+        }
+
+
         $renderarray += [
           // Enrich the render array with caching information.
           '#cache' => [
diff --git a/src/Services/BlizzDynamicEntitylistWidgetServices.php b/src/Services/BlizzDynamicEntitylistWidgetServices.php
index a9ae633..e5b0421 100644
--- a/src/Services/BlizzDynamicEntitylistWidgetServices.php
+++ b/src/Services/BlizzDynamicEntitylistWidgetServices.php
@@ -12,6 +12,10 @@ use Drupal\Core\Routing\RouteMatchInterface;
 use Drupal\Core\StringTranslation\TranslationInterface;
 use Symfony\Component\HttpFoundation\Response;
 use Zend\Diactoros\Response\JsonResponse;
+use Drupal\Core\Entity\EntityInterface;
+use Drupal\Core\Ajax\AjaxResponse;
+use Drupal\Core\Ajax\ReplaceCommand;
+use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
 
 /**
  * Class BlizzDynamicEntitylistWidgetServices.
@@ -173,17 +177,6 @@ class BlizzDynamicEntitylistWidgetServices implements BlizzDynamicEntitylistWidg
       ) + ['#prefix' => '<div class="viewmode">', '#suffix' => '</div>'];
     }
 
-    // If there is only a single bundle to choose from, pre-select this bundle
-    // and set the widget to automatically trigger the next widget step.
-    if (count($bundles) == 1 && !$raw) {
-      // Determine the value of the available option.
-      $optionvalue = array_keys($bundles);
-      $optionvalue = array_shift($optionvalue);
-      $elem['bundleselect']['#default_value'] = [$optionvalue => $optionvalue];
-      $elem['bundleselect']['#attributes']['checked'] = 'checked';
-      $elem['bundleselect']['#attributes']['data-trigger-next-step'] = 1;
-    }
-
     // If we're in an ajax call, we need to simulate a normal
     // form build procedure, otherwise (multiple) checkboxes won't
     // get generated correctly.
@@ -326,6 +319,64 @@ class BlizzDynamicEntitylistWidgetServices implements BlizzDynamicEntitylistWidg
           ],
         ],
       ],
+      // Load more checkbox.
+      'load_more' => [
+        '#type' => 'checkbox',
+        '#title' => $this->stringTranslator->translate('Load More'),
+        '#default_value' => isset($listdefinition['load_more']) ? $listdefinition['load_more'] : 0,
+        '#description' => $this->stringTranslator->translate('Provide load more by AJAX functionality.'),
+        '#description_display' => 'after',
+        '#attributes' => [
+          'class' => [
+            'blizzdynamicentitylist-selector',
+            'data-input',
+            'list-load-more',
+          ],
+        ],
+      ],
+      'number_of_load' => [
+        '#type' => 'number',
+        '#title' => $this->stringTranslator->translate('Number of items to load'),
+        '#description' => $this->stringTranslator->translate('How many entities load when click on `Load More` button.'),
+        '#value' => isset($listdefinition['number_of_load']) ? $listdefinition['number_of_load'] : 10,
+        '#min' => 1,
+        '#description_display' => 'after',
+        '#attributes' => [
+          'class' => [
+            'blizzdynamicentitylist-selector',
+            'data-input',
+            'list-number-of-load',
+          ],
+        ],
+        '#states' => [
+          'visible' => [
+            ':input[name$="[load_more]"]' => [
+              'checked' => TRUE,
+            ],
+          ],
+        ]
+      ],
+      'infinite_scroll' => [
+        '#type' => 'checkbox',
+        '#title' => $this->stringTranslator->translate('Automatically Load Content'),
+        '#description' => $this->stringTranslator->translate('Automatically load subsequent items as the user scrolls.'),
+        '#default_value' => isset($listdefinition['infinite_scroll']) ? $listdefinition['infinite_scroll'] : 0,
+        '#description_display' => 'after',
+        '#attributes' => [
+          'class' => [
+            'blizzdynamicentitylist-selector',
+            'data-input',
+            'infinite-scroll',
+          ],
+        ],
+        '#states' => [
+          'visible' => [
+            ':input[name$="[load_more]"]' => [
+              'checked' => TRUE,
+            ],
+          ],
+        ]
+      ],
       'options' => [
         '#type' => 'details',
         '#title' => $this->stringTranslator->translate('List options'),
@@ -402,6 +453,57 @@ class BlizzDynamicEntitylistWidgetServices implements BlizzDynamicEntitylistWidg
       ],
     ];
 
+    // Add Exceptional filters.
+    $elem['filters']['filtercontainer']['filtercontainer_exclude'] = [
+      '#type' => 'details',
+      '#open' => FALSE,
+      '#title' => $this->stringTranslator->translate('Exceptional filters'),
+      '#description' => $this->stringTranslator->translate('You can use these filters to exclude unnecessary categories.'),
+      '#attributes' => [
+        'class' => [
+          'filters',
+        ],
+      ],
+      '#weight' => 10,
+    ];
+
+    // Exclude entities field.
+    $elem['filters']['filtercontainer']['filtercontainer_exclude']['excluded_entities'] = [
+      '#type' => 'container',
+      '#attributes' => [
+        'class' => [
+          'filtercontainer',
+        ],
+      ],
+      'filtervalue' => [
+        '#type' => 'select',
+        // Intentionally no field name, explanation see below.
+        '#name' => FALSE,
+        '#title' => $this->stringTranslator->translate('Exclude Items'),
+        '#attributes' => [
+          'class' => [
+            'blizzdynamicentitylist-selector',
+            'autocomplete-' . $entity_type,
+            'filtervalue',
+          ],
+          'data-sourcefield' => 'excluded_entities',
+          'data-targetbundles' => implode(',', $bundle),
+          'data-multiple' => 1,
+          'data-maximumselectionlength' => -1,
+          'data-length' => 100,
+          'multiple' => 'multiple',
+        ],
+      ],
+    ];
+    // Set default value for exclude filter.
+    if (!empty($listdefinition['filters']['excluded_entities'])) {
+      $this->setFilterDefaultValue(
+        $elem['filters']['filtercontainer']['filtercontainer_exclude']['excluded_entities']['filtervalue'],
+        $entity_type,
+        $listdefinition['filters']['excluded_entities']
+      );
+    }
+
     // Taxonomy term reference fields will be defined like this:
     $taxonomyReferenceFieldCriteria = [
       'getType' => 'entity_reference',
@@ -448,7 +550,8 @@ class BlizzDynamicEntitylistWidgetServices implements BlizzDynamicEntitylistWidg
         $target_bundles = $field_definition->getSetting('handler_settings')['target_bundles'];
 
         // Add the filter field definition.
-        $elem['filters']['filtercontainer'][$field_machine_name] = [
+        $elem['filters']['filtercontainer'][$field_machine_name] =
+        $elem['filters']['filtercontainer']['filtercontainer_exclude']['blizz_excluded_' . $field_machine_name] = [
           '#type' => 'container',
           '#attributes' => [
             'class' => [
@@ -474,6 +577,9 @@ class BlizzDynamicEntitylistWidgetServices implements BlizzDynamicEntitylistWidg
           ],
         ];
 
+        $elem['filters']['filtercontainer']['filtercontainer_exclude']['blizz_excluded_' . $field_machine_name]['filtervalue']['#attributes']['data-sourcefield'] =
+          'blizz_excluded_' . $field_machine_name;
+
         /*
          * So why do the 'filtervalue' fields use no field name?
          *
@@ -495,40 +601,27 @@ class BlizzDynamicEntitylistWidgetServices implements BlizzDynamicEntitylistWidg
         if ($multiple) {
 
           // ...set an appropriate html attribute.
-          $elem['filters']['filtercontainer'][$field_machine_name]['filtervalue']['#attributes']['multiple'] = 'multiple';
+          $elem['filters']['filtercontainer'][$field_machine_name]['filtervalue']['#attributes']['multiple'] =
+          $elem['filters']['filtercontainer']['filtercontainer_exclude']['blizz_excluded_' . $field_machine_name]['filtervalue']['#attributes']['multiple'] = 'multiple';
 
         }
 
         // Prepopulate filtervalue fields with potential data.
         if (!empty($listdefinition['filters'][$field_machine_name])) {
 
-          // Shortcut for convenience...
-          $input_element = &$elem['filters']['filtercontainer'][$field_machine_name]['filtervalue'];
-
-          // Load the referenced entities to generate complete field options.
-          $terms = $this->entityServices->loadEntities(
+          $this->setFilterDefaultValue(
+            $elem['filters']['filtercontainer'][$field_machine_name]['filtervalue'],
             'taxonomy_term',
-            is_array($listdefinition['filters'][$field_machine_name])
-              ? $listdefinition['filters'][$field_machine_name]
-              : [$listdefinition['filters'][$field_machine_name]]
+            $listdefinition['filters'][$field_machine_name]
           );
+        }
+        if (!empty($listdefinition['filters']['blizz_excluded_' . $field_machine_name])) {
 
-          // Generate the chosen field options.
-          $input_element['#options'] = array_map(
-            function (ContentEntityInterface $term) {
-              return $term->label();
-            },
-            $terms
+          $this->setFilterDefaultValue(
+            $elem['filters']['filtercontainer']['filtercontainer_exclude']['blizz_excluded_' . $field_machine_name]['filtervalue'],
+            'taxonomy_term',
+            $listdefinition['filters']['blizz_excluded_' . $field_machine_name]
           );
-
-          // Set the chosen options also as default values.
-          $input_element['#default_value'] = is_array($listdefinition['filters'][$field_machine_name])
-            ? array_combine(
-                array_values($listdefinition['filters'][$field_machine_name]),
-                array_values($listdefinition['filters'][$field_machine_name])
-              )
-            : $listdefinition['filters'][$field_machine_name];
-
         }
       }
 
@@ -553,6 +646,10 @@ class BlizzDynamicEntitylistWidgetServices implements BlizzDynamicEntitylistWidg
       }
       $query->sort($labelkey, 'ASC');
     }
+    if (!empty($_GET['length'])) {
+      $query->range(0, $_GET['length']);
+    }
+
     $ids = $query->execute();
     $entities = $this->entityServices->loadEntities($entity_type, $ids);
 
@@ -596,20 +693,7 @@ class BlizzDynamicEntitylistWidgetServices implements BlizzDynamicEntitylistWidg
   public function getListDefinition(array $fieldcontents) {
     if (!empty($fieldcontents) && is_array($fieldcontents) && isset($fieldcontents['listdefinition'])) {
       if ($listdefinition = unserialize($fieldcontents['listdefinition'])) {
-        $listdefinition = array_shift($listdefinition);
-        if (empty(array_diff_key(
-          array_flip([
-            'entity_type_id',
-            'bundle',
-            'viewmode',
-            'length',
-            'offset',
-            'filters',
-          ]),
-          $listdefinition))
-        ) {
-          return $listdefinition;
-        }
+        return array_shift($listdefinition);
       }
     }
     return [];
@@ -619,6 +703,7 @@ class BlizzDynamicEntitylistWidgetServices implements BlizzDynamicEntitylistWidg
    * {@inheritdoc}
    */
   public function getListEntities(array $listdefinition, $register_ids, $preview_mode = FALSE) {
+    $parameters = $this->routeMatcher->getParameters();
 
     // Fetch the entity field definition(s).
     $entity_bundle_fields = $this->entityServices->getEntityTypeBundleFields(
@@ -642,6 +727,15 @@ class BlizzDynamicEntitylistWidgetServices implements BlizzDynamicEntitylistWidg
 
     // Are there any entities that should get excluded?
     $no_entity_exclusion = isset($listdefinition['options']->no_entity_exclusion) && $listdefinition['options']->no_entity_exclusion;
+
+    // Hack for lazy loading.
+    if (\Drupal::request()->query->has('exclude')) {
+      $additional_exclude = \Drupal::request()->query->get('exclude');
+      if (is_array($additional_exclude)) {
+        $this->api->excludeEntitiesFromLists($listdefinition['entity_type_id'], $additional_exclude);
+      }
+    }
+
     if (!$no_entity_exclusion && ($entitiesToExclude = $this->api->getExcludedEntities($listdefinition['entity_type_id']))) {
       $query->condition(
         $this->entityServices->getEntityKey($listdefinition['entity_type_id'], 'id'),
@@ -696,29 +790,75 @@ class BlizzDynamicEntitylistWidgetServices implements BlizzDynamicEntitylistWidg
     }
 
     // Determine the maximum number of entities requested.
+    $offset = $parameters->get('offset') ? $parameters->get('offset') : $listdefinition["offset"];
     if ($preview_mode) {
       // Max. 10 entities in preview mode...
       $length = ($listdefinition['length'] != 0 && $listdefinition['length'] < 10) ? $listdefinition['length'] : 10;
     }
+    elseif (isset($listdefinition['load_more']) && $listdefinition['load_more']) {
+      $length = $listdefinition['number_of_load'];
+    }
     else {
       $length = $listdefinition['length'] != 0 ? $listdefinition['length'] : 999999;
     }
 
+    // Hack for lazy loading.
+    if (\Drupal::request()->query->has('exclude')) {
+      $additional_exclude = \Drupal::request()->query->get('exclude');
+      if (is_array($additional_exclude)) {
+        $offset = 0;
+      }
+    }
+
     // Apply offset and length conditions.
     $query->range(
-      $listdefinition['offset'],
+      $offset,
       $length
     );
 
     // Apply any filters defined, but only if the fields actually exist.
     if (!empty($listdefinition['filters'])) {
+
+      // Exclude chosen entities in the filter.
+      if (!empty($listdefinition['filters']['excluded_entities'])) {
+        $query->condition(
+          $this->entityServices->getEntityKey($listdefinition['entity_type_id'], 'id'),
+          $listdefinition["filters"]["excluded_entities"],
+          'NOT IN'
+        );
+      }
+      if (isset($listdefinition['filters']['excluded_entities'])) {
+        unset($listdefinition['filters']['excluded_entities']);
+      }
+
       foreach ($listdefinition['filters'] as $field => $value) {
+        $prefix = 'blizz_excluded_';
+        $operator = is_array($value) ? 'IN' : '=';
+
+        // Check if it is excluded field and change operator.
+        $is_excluded_filter = FALSE;
+        if (strpos($field, $prefix) === 0) {
+          $field = preg_replace('/^' . preg_quote($prefix, '/') . '/', '', $field);
+          $operator = is_array($value) ? 'NOT IN' : '!=';
+          $is_excluded_filter = TRUE;
+        }
+
         if (isset($entity_bundle_fields[$field]) && !empty($value)) {
-          $query->condition(
-            $field,
-            $value,
-            is_array($value) ? 'IN' : '='
-          );
+          // Check if field is excluded then also get filed with NULL.
+          if ($is_excluded_filter) {
+            $query->condition(
+              $query->orConditionGroup()
+                ->condition($field, $value, $operator)
+                ->condition($field, NULL, 'IS NULL')
+            );
+          }
+          else {
+            $query->condition(
+              $field,
+              $value,
+              $operator
+            );
+          }
         }
       }
     }
@@ -853,6 +993,32 @@ class BlizzDynamicEntitylistWidgetServices implements BlizzDynamicEntitylistWidg
     }
   }
 
+  /**
+   * {@inheritdoc}
+   */
+  public function getListByAjax(EntityInterface $entity, $field_name, $view_mode, $language, $offset) {
+    // Ensure that this is a valid Content Entity.
+    if (!($entity instanceof ContentEntityInterface)) {
+      throw new BadRequestHttpException('Requested Entity is not a Content Entity.');
+    }
+
+    // Check that this field exists.
+    if (!$field = $entity->getTranslation($language)->get($field_name)) {
+      throw new BadRequestHttpException('Requested Field does not exist.');
+    }
+
+    $response = new AjaxResponse();
+    $field_elements = $field->view($view_mode);
+    $response->addCommand(
+      new ReplaceCommand(
+        "#ajax-list-{$entity->getEntityTypeId()}-{$entity->id()}-{$field_name}",
+        $this->renderer->render($field_elements)
+      )
+    );
+
+    return $response;
+  }
+
   /**
    * Fetches the field storage definition from the active config.
    *
@@ -867,4 +1033,40 @@ class BlizzDynamicEntitylistWidgetServices implements BlizzDynamicEntitylistWidg
     return $fieldConfig->get('settings');
   }
 
+  /**
+   * Set default value/values for filters.
+   *
+   * @param array $input_element
+   *   The element.
+   * @param string $entity_type
+   *   The type of entity.
+   * @param $default_values
+   *   The default value for select box.
+   */
+  private function setFilterDefaultValue(array &$input_element, $entity_type, $default_values) {
+    // Load the referenced entities to generate complete field options.
+    $entities = $this->entityServices->loadEntities(
+      $entity_type,
+      is_array($default_values)
+        ? $default_values
+        : [$default_values]
+    );
+
+    // Generate the chosen field options.
+    $input_element['#options'] = array_map(
+      function (ContentEntityInterface $entitiy) {
+        return $entitiy->label();
+      },
+      $entities
+    );
+
+    // Set the chosen options also as default values.
+    $input_element['#default_value'] = is_array($default_values)
+      ? array_combine(
+        array_values($default_values),
+        array_values($default_values)
+      )
+      : $default_values;
+  }
+
 }
