
Example for the *.extension :

global $_nimbus_config_override_directories_regex;
$_nimbus_config_override_directories_regex = [
  "/.*.extension/"
];


// Nimbus config override settings.
global $_nimbus_config_override_directories;
$_nimbus_config_override_directories = [
  [
    'path' => '../config/shared',
    'class' => '\Drupal\nimbus\Storages\FilterStorage'
  ],
  [
    'path' => '../config/override',
    'class' => '\Drupal\nimbus\Storages\FilterStorage'
  ],
  [
    'path' => '../config/local',
    'class' => '\Drupal\nimbus\Storages\FilterStorage'
  ],
  [
    'path' => '../config/export',
    'class' => '\Drupal\nimbus\Storages\FilterStorage'
  ],
];

